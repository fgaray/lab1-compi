/*CODIGO JFLEX*/
/*AREA DE IMPORTACION*/
import java.io.*;
import java_cup.runtime.*;

/*CLASE DATOS*/

class Datos{
	public static String nombreArchivo = ""; //contendra el nombre del codigo que se esta revizando	
	public static String palabra = ""; //el token que se esta revizando
	public static String codigoCorrecto = ""; //el codigo para la impresion
	public static int fila = 1; //fila actual
	public static int columna = 1; //columna actual
        public static int lineas = 0;
        public static int errores_lexicos = 0;
        public static int errores_sintacticos = 0;

	//metodo que escribe la salida en un archivo e texto: "entrada.txt"
	public static void escribe(String archivo, String textos){
		try{          
				FileWriter file1 = new FileWriter(archivo+".txt");

				BufferedWriter buff = new BufferedWriter(file1);

				buff.write(textos);
				buff.newLine();

				buff.close();
				file1.close();

                                if(errores_lexicos == 0 && errores_sintacticos == 0)
                                {
                                        System.out.println("No hay errores.");
                                }else{
                                        
                                        if(errores_lexicos > 0)
                                        {
                                                System.out.println(errores_lexicos + " errores lexicos encontrados");
                                        }

                                        if(errores_sintacticos > 0)
                                        {
                                                System.out.println(errores_sintacticos + " errores sintáctico encontrados");
                                        }

                                }
                                
                                System.out.println("Se imprimio el codigo correcto en: "+archivo+".txt");
		}
		catch(IOException e){
				System.out.println("<"+archivo+"> Error: no se puede escribir");
		}
	}

        public static void manejoLexema(String yytext, int yyline, int yycolumn)
        {
                lineas = 0;

                if(!palabra.equals(""))
                {
                        codigoCorrecto += palabra+" ";	
                }

                palabra = yytext;
                fila = yyline;
                columna = yycolumn;
        }
}

%%

%line
%column
%cup
%unicode

%{
        Datos datos = new Datos();
%}

%eof{
	//datos.codigoCorrecto += "\n";	
%eof}

/*expresiones regulares del lab*/

palabra = [a-z][a-z]*


/*Expresiones regulares para otras cosas*/

blancos = " " | "\t"


/*AREA DE REGLAS*/
%%


<YYINITIAL>
{

        "key" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.T, new String(yytext()));
        }
		
        "+" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.MAS, new String(yytext()));
        }
		
        "_" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.UNDERLINE, new String(yytext()));
        }
		
        "*" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.CLAUSURA, new String(yytext()));
        }


        "(" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.ABRE_PARENTESIS, new String(yytext()));
        }

        ")" {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.CIERRA_PARENTESIS, new String(yytext()));
        }

        {palabra} {
                datos.manejoLexema(yytext(), yyline, yycolumn);
                return new Symbol(sym.R, new String(yytext()));
        }

        {blancos} {
                //un espacio, no hacemos nada
                datos.lineas = 0;
        }


        "\n" {
                //salto de linea
                if(datos.lineas ==  0)
                {
                        datos.codigoCorrecto += datos.palabra+" ";	
                        datos.palabra = "\n";
                }

                datos.lineas++;
        }
}

[^] {	
        System.out.println("<"+datos.nombreArchivo+"> Error Léxico ("+yyline+","+yycolumn+"): Lexema: <"+yytext()+"> no válido");

        datos.errores_lexicos++;
}
	
